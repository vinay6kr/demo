package com.egnize.demosearch.utils;

import com.egnize.demosearch.constants.AppConstants;

import java.io.File;
import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by VINAY'S on 6/27/2017.
 */

public class PostData {
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    @Nullable
    private static String getApiCall(String url) throws IOException {

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("user-key", AppConstants.API_KEY)
                .build();

        Response response = client.newCall(request).execute();
            return response.body().string();
    }

    public static Observable<String> getJson(final String url) {

        return Observable.defer(() -> {
            // Do some long running operation

            try {
                return Observable.just(getApiCall(url));
            } catch (IOException e) {
                return null;
            }

        });
    }





}
