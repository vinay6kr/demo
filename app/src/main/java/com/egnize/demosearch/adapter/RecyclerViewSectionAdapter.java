package com.egnize.demosearch.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.egnize.demosearch.R;
import com.egnize.models.response.Restaurant;
import com.egnize.models.response.Restaurant_;
import com.egnize.models.section.Category;
import com.egnize.models.section.Datum;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RecyclerViewSectionAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    private List<Restaurant> allData;

    private List<Restaurant> categoryItemList = new ArrayList<>();

    private int totalPrice ;
    private int totalItems ;
    private String sectionName;


    public RecyclerViewSectionAdapter(List<Restaurant> data) {

        this.allData = data;

    }


    @Override
    public int getSectionCount() {
        return allData.size();
    }

    @Override
    public int getItemCount(int section) {

        return allData.size();

    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {

         sectionName = allData.get(section).getRestaurant().getCuisines();
        SectionViewHolder sectionViewHolder = (SectionViewHolder) holder;
        sectionViewHolder.sectionTitle.setText(sectionName);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int section, int relativePosition, int absolutePosition) {




        Restaurant_ itemsInSection = allData.get(section).getRestaurant();
        if (sectionName.equals(itemsInSection.getCuisines())){
            String itemName = itemsInSection.getName();

            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;


            itemViewHolder.txtServiceName.setText(itemName);

        }





        // Try to put a image . for sample i set background color in xml layout file
        // itemViewHolder.itemImage.setBackgroundColor(Color.parseColor("#01579b"));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, boolean header) {
        View v = null;
        if (header)

        {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.header, parent, false);
            return new SectionViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.items, parent, false);
            return new ItemViewHolder(v);
        }

    }


    // SectionViewHolder Class for Sections
    public static class SectionViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtSectionTitle) TextView sectionTitle;


        private SectionViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    // ItemViewHolder Class for Items in each Section
    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textView) TextView txtServiceName;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }
}