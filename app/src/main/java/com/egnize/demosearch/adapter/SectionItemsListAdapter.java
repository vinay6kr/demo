package com.egnize.demosearch.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.egnize.demosearch.R;
import com.egnize.models.response.Restaurant;
import com.egnize.models.response.Restaurant_;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by VINAY'S on 26/05/18.
 */
public class SectionItemsListAdapter extends RecyclerView.Adapter<SectionItemsListAdapter.ViewHolder> {
    List<Restaurant> restaurants;
    List<String> stringList;

    Context mContext;
    public SectionItemsListAdapter(Context mContext, List<Restaurant> restaurants) {
        this.restaurants = restaurants;
        this.mContext = mContext;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.header, parent, false);
        final ViewHolder mViewHolder = new ViewHolder(mView);

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Restaurant_ restaurant = restaurants.get(position).getRestaurant();
        String section = restaurant.getCuisines();
        holder.txtSectionTitle.setText(restaurant.getCuisines());

        holder.rvSection.setLayoutManager(new LinearLayoutManager(mContext));


        for (int i = 0; i < restaurants.size(); i++) {
            if (section.equals(restaurants.get(i).getRestaurant().getCuisines())){
                ItemsListAdapter adapter = new ItemsListAdapter(mContext, restaurants, i);
                holder.rvSection.setAdapter(adapter);
            }
        }


//        holder.tvRating.setText(restaurant.getUserRating().getRatingText());
//        holder.tvRating.setBackgroundColor(Color.parseColor("#"+restaurant.getUserRating().getRatingColor()));
////
//        holder.tvLocation.setText(restaurant.getLocation().getAddress());
//        holder.tvPrice.setText(restaurant.getCurrency()+restaurant.getPriceRange()+" for two people (approx)");
//
//        Glide.with(mContext).load(restaurant.getFeaturedImage()).into(holder.imageView);
    }


    @Override
    public int getItemCount() {

        return restaurants.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rvSection) RecyclerView rvSection;
        @BindView(R.id.txtSectionTitle) TextView txtSectionTitle;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}