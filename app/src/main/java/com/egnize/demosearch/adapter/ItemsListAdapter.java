package com.egnize.demosearch.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.egnize.demosearch.R;
import com.egnize.models.response.Restaurant;
import com.egnize.models.response.Restaurant_;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by VINAY'S on 26/05/18.
 */
public class ItemsListAdapter extends RecyclerView.Adapter<ItemsListAdapter.ViewHolder> {
    List<Restaurant> restaurants;
    int itemPos;

    Context mContext;
    private List<Restaurant> restaurantss;

    public ItemsListAdapter(Context mContext, List<Restaurant> restaurants, int itemPos) {
        this.restaurants = restaurants;
        this.mContext = mContext;
        this.itemPos = itemPos;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.items, parent, false);
        final ViewHolder mViewHolder = new ViewHolder(mView);

        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Restaurant_ restaurant = restaurantss.get(position).getRestaurant();
        holder.textView.setText(restaurant.getName());
        holder.tvRating.setText(restaurant.getUserRating().getRatingText());
        holder.tvRating.setBackgroundColor(Color.parseColor("#"+restaurant.getUserRating().getRatingColor()));
//
        holder.tvLocation.setText(restaurant.getLocation().getAddress());
        holder.tvPrice.setText(restaurant.getCurrency()+restaurant.getAverageCostForTwo()+" for two people (approx)");

        Glide.with(mContext).load(restaurant.getFeaturedImage()).into(holder.imageView);


    }


    @Override
    public int getItemCount() {
        restaurantss=new ArrayList<>();
        for (int i = 0; i < restaurants.size(); i++) {
            if (restaurants.get(itemPos).getRestaurant().getCuisines().equals(restaurants.get(i).getRestaurant().getCuisines())){
                restaurantss.add(restaurants.get(i));
            }
        }
        return restaurantss.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textView)
        TextView textView;
        @BindView(R.id.tvLocation) TextView tvLocation;
        @BindView(R.id.tvPrice) TextView tvPrice;
        @BindView(R.id.imageView)
        AppCompatImageView imageView;
        @BindView(R.id.tvRating) TextView tvRating;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}