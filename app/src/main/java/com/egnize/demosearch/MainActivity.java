package com.egnize.demosearch;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.egnize.demosearch.adapter.ItemsListAdapter;
import com.egnize.demosearch.adapter.RecyclerViewSectionAdapter;
import com.egnize.demosearch.adapter.SectionItemsListAdapter;
import com.egnize.demosearch.adapter.SectionedRecyclerViewAdapter;
import com.egnize.demosearch.constants.AppConstants;
import com.egnize.demosearch.utils.Logger;
import com.egnize.demosearch.utils.PostData;
import com.egnize.models.response.ResponseData;
import com.egnize.models.response.Restaurant;
import com.egnize.models.section.Category;
import com.egnize.models.section.Datum;
import com.egnize.models.section.SectionData;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import static com.egnize.demosearch.utils.Utils.dismissProgress;
import static com.egnize.demosearch.utils.Utils.showProgress;

/**
 * Created by VINAY'S on 26/05/18.
 */
public class MainActivity extends AppCompatActivity {
    private final CompositeDisposable disposables = new CompositeDisposable();

    private static final String TAG = "@VK::";
    @BindView(R.id.rv)
    RecyclerView rv;
    @BindView(R.id.txtSearch)
    TextInputEditText txtSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        rv.setLayoutManager(new LinearLayoutManager(this));







        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchcall();
            }
        });


        txtSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (txtSearch.getRight() - txtSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                        txtSearch.setText("");
                        return true;
                    }
                }
                return false;
            }
        });




    }

    private void searchcall() {

        String text = txtSearch.getText().toString();
        if (!TextUtils.isEmpty(text)){

            String url = AppConstants.BASE_URL+text;
            getRxCall(url);



        }else {
            Toast.makeText(this, "Enter Key word to search!", Toast.LENGTH_SHORT).show();
        }

    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getRxCall(String url) {
        disposables.add(PostData.getJson(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<String>() {
                    @Override
                    protected void onStart() {
                        super.onStart();
                        showProgress(MainActivity.this);
                    }

                    @Override
                    public void onNext(@NonNull String res) {
                        Logger.d(TAG, "Response::"+res);
                        try {

                            ResponseData responseData = new Gson().fromJson(res, ResponseData.class);


                            SectionData sectionData = new SectionData();
                            List<Datum> datumList = new ArrayList<>();
//                            List<Category> categoryList = new ArrayList<>();

                            List<Restaurant> restaurantList = responseData.getRestaurants();


                            for (int i = 0; i < restaurantList.size(); i++) {
                                List<Category> categoryList = new ArrayList<>();
                                categoryList.add(new Category(restaurantList.get(i).getRestaurant().getName()));
                                datumList.add(new Datum(restaurantList.get(i).getRestaurant().getCuisines(),categoryList));

                            }

//
//                            List<Datum> newdatumList = new ArrayList<>();
//                            for (int i = 0; i < datumList.size(); i++) {
//                                for (int j = 0; j < datumList.size(); j++) {
//                                    if (newdatumList.size()>0){
//
//
//                                        if (newdatumList.contains(datumList.get(j))){
//                                            int removePos =  newdatumList.indexOf(datumList.get(j).getSectionName());
//                                            List<Category> categoryList = new ArrayList<>();
//                                            categoryList.add(new Category(restaurantList.get(j).getRestaurant().getName()));
//
//                                            newdatumList.get(removePos).setCategory(categoryList );
//
//                                        }else {
//                                            newdatumList.add(datumList.get(j));
//                                        }
//                                    }else {
//                                        newdatumList.add(datumList.get(i));
//
//                                    }
//
//                                }
//                            }

//
//                            List<Datum> newdatumList = new ArrayList<>();
//                            for (int i = 0; i < datumList.size(); i++) {
//                                String section = datumList.get(i).getSectionName();
//                                if ()
//                                for (int j = 0; j < datumList.size(); j++) {
//                                    if (newdatumList.size()>0){
//                                        if (datumList.equals(datumList.get(j).getSectionName())){
//                                            List<Category> categoryList = new ArrayList<>();
//                                            categoryList.add(new Category(restaurantList.get(i).getRestaurant().getName()));
//
//                                            newdatumList.add(new Datum(restaurantList.get(i).getRestaurant().getCuisines(),categoryList));
//
//                                        }else {
//                                            List<Category> categoryList = new ArrayList<>();
//                                            categoryList.add(new Category(restaurantList.get(i).getRestaurant().getName()));
//                                        }
//                                    }
//
//                                }
//
//
//                            }






//                            RecyclerViewSectionAdapter adapter = new RecyclerViewSectionAdapter(responseData.getRestaurants() );

                            SectionItemsListAdapter adapter = new SectionItemsListAdapter(MainActivity.this , responseData.getRestaurants());
                            rv.setAdapter(adapter);


                        }catch (Exception e){
                            Toast.makeText(MainActivity.this, "Server Error!", Toast.LENGTH_SHORT).show();
                        }


                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        dismissProgress();

                    }

                    @Override
                    public void onComplete() {
                        dismissProgress();
                    }
                }));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposables.clear();
    }
}
