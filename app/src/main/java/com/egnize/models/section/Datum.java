
package com.egnize.models.section;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("section_name")
    @Expose
    private String sectionName;
    @SerializedName("category")
    @Expose
    private List<Category> category = null;

    public Datum(String sectionName, List<Category> category) {
        this.sectionName = sectionName;
        this.category = category;
    }

    public Datum() {
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

}
